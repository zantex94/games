'use strict';
/* Downey: thinkjava, ex 71 */
const loop = function(n) {
    console.log("Variable\tIteration\toutput");
    console.log(n);
    let i = n;
    /* the loop only terminates positive numbers. the loop can´t accept any negative number.*/
    while (i > 1) {
        console.log(n + '\t'+ '\t' + '\t'+ '\t'+ i);
        if (i % 2 == 0) {
            i = i / 2;
            console.log( " "+ '\t'+ '\t' + '\t'+ '\t'+ '\t'+ '\t' + '\t' +i);
        } else {
            i = i + 1;
            console.log( " "+ '\t'+ '\t' + '\t'+ '\t' + '\t'+ '\t'+ '\t' + i);
        }
    }

}

