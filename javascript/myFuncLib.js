'use strict'
/* this function get factorial by iteration. (like to do that more than recursive functions! =) */
function facti(n){
    let result = 1
    do{
        result = result * n
        n--;
    }while(n > 1)
    return result;
}
/* this function get r^n by iteration.*/
function powi(r,e){
    let result = 1;
    while(e > 0){
        result = result * r;
        e--;
    }
    return result;

}
