'use strict'
function doSomething () {
    let form1 = document.getElementById("form1");
    form1.addEventListener('roolDiceButton', function(event){
        event.preventDefault()
      });
}


window.addEventListener('load', doSomething);   
/* This function rools a dice with a random number */

function roolDice(n){
    let ones = 0;
    let twos = 0;
    let threes = 0;
    let fours = 0;
    let fives = 0;
    let sixes = 0;
    let i = 1;
   

    let rollingTimes = n;
    if(rollingTimes != 0){
        /* the loop runs x roilling times that the users choose */
        while (i <= rollingTimes){
            let rolled = Math.floor(Math.random() * 6 + 1);
            switch(rolled){
                case 1: 
                ones++;
                break;
                case 2: 
                twos++;
                break;
                case 3: 
                threes++;
                break;
                case 4: 
                fours++;
                break;
                case 5: 
                fives++;
                break;
                default: 
                sixes++;
        
            }   
           
            i++;
        }
        return document.getElementById('RoolResult').innerHTML = "Result for possible outcomes: 1: " + ones + ", 2: " + twos + ", 3: " + threes + ", 4: " + fours + ", 5: " + fives + ", 6: " + sixes;
    }
        
    }

