'use strict'
/* this fuunction calculates fatorial ! */
function fact1(n) {
    if (n == 0 || n == 1) {
        return 1;
    } else {
        console.log(n);
        return n * fact1(n - 1);
    }
  
}
/* fabonacci is a number that increase like 0 + 1 = 2 + 1 = 3 + 2 = 5 and so on. */
function fibonacci(num) {
    if(num < 3) {
        return 1;
    }
    else {
        return fibonacci(num-1) + fibonacci(num - 2);
    }
}
/* this function takes in R^N Esk. 4^2 = 16.*/
function pow(r,e) {
    if (e <= 0) 
   {
    return 1;
    }
  else 
  {
    return r * pow(r, e-1);
  }
}
/* this function checks if a word can be seen as palindrome. */
function isItPalidrome(str) {
    /* if the string is empty it is true*/
    if (str.length === 0){
        return true;
    }
    /* checking that the first character and last are not identical. */
    if (str[0] !== str[str.length-1]){
        return false;
    }
    /* we slice the first and last character */
    return isItPalidrome(str.slice(1, str.length-1))

  }

