'use strict'
/* this function calculates if a number is a prime number. */
function isPrime(p){
    let i = 2;
    while(i< p){
        if(p % i === 0){
            return false;
        }
        i++;
    }
return true;

}